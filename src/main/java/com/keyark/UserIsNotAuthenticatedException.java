/*
 **********************************************************************************
 * Copyright (C) 2017-present KEYARK, INC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
package com.keyark;

import java.io.Serial;

/**
 * @author Konstantin Andreyev kandreyev@keyark.com
 *
 */
public class UserIsNotAuthenticatedException extends KSException {
	@Serial
	private static final long serialVersionUID = -5006519030597155936L;

	/**
	 * @param message - error message
	 * @param cause   - cause exception
	 * @param code    - error code
	 */
	public UserIsNotAuthenticatedException(String message, Throwable cause, int code) {
		super(message, cause, code);
	}

	/**
	 * @param message - error message
	 * @param code    - error code
	 */
	public UserIsNotAuthenticatedException(String message, int code) {
		super(message, code);
	}

	/**
	 * @param code - error code
	 */
	public UserIsNotAuthenticatedException(int code) {
		super(code);
	}

	/**
	 * @param message - error message
	 */
	public UserIsNotAuthenticatedException(String message) {
		super(message);
	}

	/**
	 * @param cause - cause exception
	 * @param code  - error code
	 */
	public UserIsNotAuthenticatedException(Throwable cause, int code) {
		super(cause, code);
	}

	/**
	 * @param cause - cause exception
	 */
	public UserIsNotAuthenticatedException(Throwable cause) {
		super(cause);
	}

	/**
	 *
	 */
	public UserIsNotAuthenticatedException() {
		this(500);
	}

}

/*
 **********************************************************************************
 * Copyright (C) 2017-present KEYARK, INC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
