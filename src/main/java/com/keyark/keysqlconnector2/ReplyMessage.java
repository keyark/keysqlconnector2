/*
 **********************************************************************************
 * Copyright (C) 2017-present KEYARK, INC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
package com.keyark.keysqlconnector2;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.keyark.KSException;

/**
 * @author Konstantin Andreyev kandreyev@keyark.com
 *
 */
public final class ReplyMessage implements Constants {
	private static final Logger logger = LoggerFactory.getLogger(ReplyMessage.class);
	private final List<ReplyPage> replyPages;

	/**
	 * @param in - input stream
	 * @throws IOException - {@link IOException}
	 *
	 */
	public ReplyMessage(final InputStream in) throws IOException {
		List<ReplyPage> pages = new ArrayList<>();
		String firstLine = null;
		try {
			int pageNum = 1;
			int queryNum = 1;
			while (pageNum > 0 || queryNum > 0) {
				firstLine = readLine(in);
				logger.debug("page header: " + firstLine);
				MessageHeader header = new MessageHeader(firstLine);
				byte[] pageData = new byte[(int) header.getMessageLength()];
				int off = 0;
				int len = pageData.length;
				int numRead = -1;
				while ((numRead = in.read(pageData, off, len)) != -1 && len > 0) {
					off += numRead;
					len -= numRead;
				}
				int separator = in.read();//read \n page separator which is not included in page data
				if (separator != '\n') {
					throw new IOException("page separator is not found at the end of the page. Page header is: " + firstLine);
				}
				ReplyPage page = new ReplyPage(header, new String(pageData, StandardCharsets.UTF_8));
				pageNum = header.getPage();
				queryNum = header.getQuery();
				pages.add(page);
			}
		} catch (Throwable t) {
			//			pages = Arrays.asList(new ReplyPage(new MessageHeader(EXIT_CODE_FATAL_ERROR), firstLine));
			logger.error("", t);
			long available = 0;
			while ((available = in.available()) > 0) {
				in.skip(available);
			}
			String msg = (t.getMessage() == null || t.getMessage().isBlank()) ? t.toString() : t.getMessage();
			throw new KSException(msg + " (page header: \"" + firstLine + "\")");
		}
		//
		this.replyPages = Collections.unmodifiableList(pages);
	}

	private String readLine(InputStream in) throws IOException {
		StringBuffer sb = new StringBuffer();
		int ch = -1;
		while ((ch = in.read()) != -1) {
			sb.append((char) ch);
			if (ch == 10) {
				break;
			}
		}
		//
		return sb.toString();
	}

	/**
	 * @return .
	 */
	public List<ReplyPage> getReplyPages() {
		return replyPages;
	}

	/**
	 * @return .
	 */
	public String getReply() {
		StringBuilder sb = new StringBuilder();
		for (ReplyPage replayPage : replyPages) {
			sb.append(replayPage.getReply());
		}
		//
		return sb.toString();
	}

	/**
	 * @return .
	 */
	public boolean isSuccess() {
		return replyPages.size() > 0 && replyPages.get(0).getHeader().getExitCode() == EXIT_CODE_SUCCESS;
	}

	/**
	 * @return .
	 */
	public boolean isFatalError() {
		return replyPages.size() > 0 && replyPages.get(0).getHeader().getExitCode() == EXIT_CODE_FATAL_ERROR;
	}

	/**
	 * @return .
	 */
	public long getClientMessageId() {
		return replyPages.size() == 0 ? -1 : replyPages.get(0).getHeader().getClientMessageId();
	}

	/**
	 * @return .
	 */
	public StatementType getStatementType() {
		return replyPages.size() == 0 ? null : replyPages.get(0).getHeader().getStatementType();
	}

	/**
	 * @return .
	 */
	public long getTotalInstanceCount() {
		return replyPages.size() == 0 ? 0 : replyPages.get(replyPages.size() - 1).getHeader().getInstanceCount();
	}

	/**
	 * @return .
	 */
	public double getElapsedTime() {
		return replyPages.size() == 0 ? 0 : replyPages.get(0).getHeader().getElapsedTime();
	}

	/**
	 * @return .
	 */
	public double getTotalTime() {
		double totalTime = 0.0;
		for (ReplyPage replayPage : replyPages) {
			totalTime += replayPage.getHeader().getElapsedTime();
		}
		//
		return totalTime;
	}

	/**
	 * @return .
	 */
	public MessageFormat getMessageFormat() {
		return replyPages.size() == 0 ? null : replyPages.get(0).getHeader().getMessageFormat();
	}

	/**
	 * @return .
	 */
	public String getEncoding() {
		return replyPages.size() == 0 ? null : replyPages.get(0).getHeader().getEncoding();
	}

	/**
	 * @return .
	 */
	public ReplyObj getReplyObj() {
		return replyPages.size() == 0 ? null : replyPages.get(0).getHeader().getSrc();
	}

	/**
	 * @return .
	 */
	public boolean isJsonReply() {
		return replyPages.size() == 0 ? false : replyPages.get(0).getHeader().getMessageFormat() == MessageFormat.json;
	}
}

/*
 **********************************************************************************
 * Copyright (C) 2017-present KEYARK, INC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
