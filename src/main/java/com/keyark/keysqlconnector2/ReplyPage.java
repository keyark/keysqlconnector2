/*
 **********************************************************************************
 * Copyright (C) 2017-present KEYARK, INC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
package com.keyark.keysqlconnector2;

/**
 * @author Konstantin Andreyev kandreyev@keyark.com
 *
 */
public final class ReplyPage {
	private final MessageHeader header;
	private final String reply;

	/**
	 * @param header - message header
	 * @param reply  - reply string
	 */
	public ReplyPage(MessageHeader header, String reply) {
		this.header = header;
		this.reply = reply;
	}

	/**
	 * @return .
	 */
	public MessageHeader getHeader() {
		return header;
	}

	/**
	 * @return .
	 */
	public String getReply() {
		return reply;
	}

	/**
	 * @return .
	 */
	public int getPageNumber() {
		return header == null ? 0 : header.getPage();
	}
}

/*
 **********************************************************************************
 * Copyright (C) 2017-present KEYARK, INC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
