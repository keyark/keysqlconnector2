/*
 **********************************************************************************
 * Copyright (C) 2017-present KEYARK, INC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
package com.keyark.keysqlconnector2;

/**
 * @author Konstantin Andreyev kandreyev@keyark.com
 *
 */
public enum ReplyObj {
	/**
	 *
	 */
	UNKNOWN(0),
	/**
	 *
	 */
	CATALOG(1),
	/**
	 *
	 */
	STORE(2),
	/**
	 *
	 */
	KEYOBJECT(3),
	/**
	 *
	 */
	CATALOGS(4),
	/**
	 *
	 */
	STORES(5),
	/**
	 *
	 */
	SCHEMA(6),
	/**
	 *
	 */
	SCHEMAS(7),
	/**
	 *
	 */
	USER(8),
	/**
	 *
	 */
	KEYOBJECTS(9),
	/**
	 *
	 */
	PERMISSIONS(10),
	/**
	 *
	 */
	PASSWORD_POLICY(11),
	/**
	 *
	 */
	SEQUENCE(12),
	/**
	 *
	 */
	STORAGE(13),
	/**
	 *
	 */
	SCRIPT(14),
	/**
	 *
	 */
	TRANSACTION(15),
	/**
	 *
	 */
	CONN_STR(16),
	/**
	 *
	 */
	OUTPUT_FORMAT(17);

	private final int val;

	/**
	 * @param val .
	 */
	ReplyObj(int val) {
		this.val = val;
	}

//	/**
//	 * @return .
//	 */
//	int valueOf() {
//		return val;
//	}

	/**
	 * @param val .
	 * @return .
	 */
	static ReplyObj valueOf(int val) {
		return switch (val) {
		case 0 -> ReplyObj.UNKNOWN;
		case 1 -> ReplyObj.CATALOG;
		case 2 -> ReplyObj.STORE;
		case 3 -> ReplyObj.KEYOBJECT;
		case 4 -> ReplyObj.CATALOGS;
		case 5 -> ReplyObj.STORES;
		case 6 -> ReplyObj.SCHEMA;
		case 7 -> ReplyObj.SCHEMAS;
		case 8 -> ReplyObj.USER;
		case 9 -> ReplyObj.KEYOBJECTS;
		case 10 -> ReplyObj.PERMISSIONS;
		case 11 -> ReplyObj.PASSWORD_POLICY;
		case 12 -> ReplyObj.SEQUENCE;
		case 13 -> ReplyObj.STORAGE;
		case 14 -> ReplyObj.SCRIPT;
		case 15 -> ReplyObj.TRANSACTION;
		case 16 -> ReplyObj.CONN_STR;
		case 17 -> ReplyObj.OUTPUT_FORMAT;
		default -> throw new IllegalArgumentException("Unknown ReplyObj value=" + val);
		};
	}
	/*
	 * 2023-05-06
	 * common.hpp
	enum class statement_obj_t {
	unknown_st = 0,
	catalog_st = 1,
	store_st = 2,
	keyobject_st = 3,
	catalogs_st = 4,
	stores_st = 5,
	schema_st = 6,
	schemas_st = 7,
	user_st = 8,
//	users_st = 9, // useless for generic statement
	keyobjects_st = 9,
	permissions_st = 10,
	password_policy_st = 11,
	sequence_st = 12,
	storage_st = 13,
	script_st = 14,
	transaction_st = 15,
	conn_str_st = 16,
	output_format = 17
	};

	 */
	/*
	 * 2022-08-30
	 * common.hpp
	enum class statement_obj_t {
	unknown_st = 0,
	catalog_st = 1,
	store_st = 2,
	keyobject_st = 3,
	catalogs_st = 4,
	stores_st = 5,
	schema_st = 6,
	schemas_st = 7,
	user_st = 8,
	//	users_st = 9, // useless for generic statement
	keyobjects_st = 9,
	permissions_st = 10,
	password_policy_st = 11,
	sequence_st = 12,
	storage_st = 13,
	script_st = 14,
	transaction_st = 15,
	conn_str_st = 16
	};
	 */
	/*
	 * 05/31/2021
	 * common.hpp
	enum class statement_obj_t {
		unknown_st = 0,
		catalog_st = 1,
		store_st = 2,
		keyobject_st = 3,
		catalogs_st = 4,
		stores_st = 5,
		schema_st = 6,
		schemas_st = 7,
		user_st = 8,
		//	users_st = 9, // useless for generic statement
		keyobjects_st = 9,
		permissions_st = 10,
		password_policy_st = 11,
		sequence_st = 12
	};

	 */
	/*
	 * 02/17/2021
	 enum class statement_obj_t {
	   unknown_st = 0,
	   catalog_st = 1,
	   store_st = 2,
	   keyobject_st = 3,
	   catalogs_st = 4,
	   stores_st = 5,
	   schema_st = 6,
	   schemas_st = 7,
	   user_st = 8,  //TODO delete: useless for generic statement but still used in permission_statement
	//     users_st = 9, // useless for generic statement
	   keyobjects_st = 9,
	   permissions_st = 10,
	   primitive_keyobject_st = 11
	};
	 */
}
/*
 **********************************************************************************
 * Copyright (C) 2017-present KEYARK, INC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
