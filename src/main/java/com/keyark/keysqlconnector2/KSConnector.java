/*
 **********************************************************************************
 * Copyright (C) 2017-present KEYARK, INC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
package com.keyark.keysqlconnector2;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Konstantin Andreyev kandreyev@keyark.com
 *
 */
public interface KSConnector {

	/**
	 * @return true if response was received, false otherwise
	 * @throws IOException .
	 */
	boolean ping() throws IOException;

	/**
	 * @param userName .
	 * @param pwd .
	 * @return result .
	 * @throws IOException .
	 */
	String bindUser(String userName, String pwd) throws IOException;

	/**
	 * @param userName .
	 * @param pwd .
	 * @param schema .
	 * @return .
	 * @throws IOException .
	 */
	String bindUser(String userName, String pwd, String schema) throws IOException;

	/**
	 * @param userName .
	 * @param oldPwd .
	 * @param newPwd .
	 * @return .
	 * @throws IOException .
	 */
	String changePassword(String userName, String oldPwd, String newPwd) throws IOException;

	/**
	 * @param dbschema .
	 * @return .
	 * @throws IOException .
	 */
	String reconnect(String dbschema) throws IOException;

	/**
	 * @return .
	 * @throws IOException .
	 */
	String logout() throws IOException;

	/**
	 * @param parQuery .
	 * @param messageFormat .
	 * @return .
	 * @throws IOException .
	 */
	ReplyMessage submit(String parQuery, MessageFormat messageFormat) throws IOException;

	/**
	 * @param parQuery .
	 * @param messageFormat .
	 * @param output .
	 * @throws IOException .
	 */
	void submit(String parQuery, MessageFormat messageFormat, OutputStream output) throws IOException;

	/**
	 * @throws IOException .
	 */
	void abort() throws IOException;

	/**
	 * submitAndGetFirstPage Acquires the lock which will be released in getNextPage() when last page reached.
	 *
	 * @param parQuery      - request query
	 * @param messageFormat - message format for response - ksql or json
	 * @return not null ReplyPage
	 *
	 */
	ReplyPage submitAndGetFirstPage(String parQuery, MessageFormat messageFormat);

	/**
	 *
	 * @return next Page
	 */
	ReplyPage getNextPage();

	/**
	 *
	 */
	void gracefullyClose();

	/**
	 *
	 */
	void close();

	/**
	 * @return true if connector's socket is closed, false otherwise
	 */
	boolean isClosed();

	/**
	 * @return true if connection authenticated, false otherwise
	 */
	boolean isAuthenticated();

}
/*
 **********************************************************************************
 * Copyright (C) 2017-present KEYARK, INC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
