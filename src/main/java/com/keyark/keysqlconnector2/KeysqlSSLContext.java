/*
 **********************************************************************************
 * Copyright (C) 2017-present KEYARK, INC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
package com.keyark.keysqlconnector2;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.cert.CertificateFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

/**
 * A factory class which creates an {@link SSLContext} that naively accepts KeySQL certificate.
 *
 * <pre>
 * // Create an SSL context that naively accepts KeySQL certificate.
 * SSLContext context = KeysqlSSLContext.getInstance("TLS");
 *
 * // Create a socket factory from the SSL context.
 * SSLSocketFactory factory = context.getSocketFactory();
 *
 * // Create a socket from the socket factory.
 * SSLSocket socket = factory.createSocket("www.example.com", 443);
 * </pre>
 *
 */
public final class KeysqlSSLContext implements Constants {
	private KeysqlSSLContext() {
	}

	/**
	 * Returns SSLContext that implements the specified secure socket protocol and naively accepts KeySQL certificate.
	 *
	 * @param protocol - the standard name of the requested protocol.See the SSLContext section in the Java Cryptography
	 *                 Architecture Standard Algorithm NameDocumentationfor information about standard protocol names.
	 * @return - the new SSLContext object
	 * @throws NoSuchAlgorithmException - {@link NoSuchAlgorithmException}
	 */
	public static SSLContext getInstance(String protocol) throws NoSuchAlgorithmException {
		return init(SSLContext.getInstance(protocol));
	}

	/**
	 * Returns SSLContext that implements the specified secure socket protocol and naively accepts KeySQL certificate.
	 *
	 * @param protocol - the standard name of the requested protocol.See the SSLContext section in the Java Cryptography
	 *                 Architecture Standard Algorithm NameDocumentationfor information about standard protocol names.
	 * @param provider - an instance of the provider.
	 * @return - the new SSLContext object
	 * @throws NoSuchAlgorithmException - {@link NoSuchAlgorithmException}
	 */
	public static SSLContext getInstance(String protocol, Provider provider) throws NoSuchAlgorithmException {
		return init(SSLContext.getInstance(protocol, provider));
	}

	/**
	 * Returns SSLContext that implements the specified secure socket protocol and naively accepts KeySQL certificate.
	 *
	 * @param protocol - the standard name of the requested protocol.See the SSLContext section in the Java Cryptography
	 *                 Architecture Standard Algorithm NameDocumentationfor information about standard protocol names.
	 * @param provider - an instance of the provider.
	 * @return - the new SSLContext object
	 * @throws NoSuchProviderException  - {@link NoSuchProviderException}
	 * @throws NoSuchAlgorithmException - {@link NoSuchAlgorithmException}
	 */
	public static SSLContext getInstance(String protocol, String provider) throws NoSuchAlgorithmException, NoSuchProviderException {
		return init(SSLContext.getInstance(protocol, provider));
	}

	/**
	 * Set KeySQL TrustManager to the given context.
	 *
	 * @param context - a secure socket protocol implementation
	 * @throws GeneralSecurityException
	 * @throws IOException
	 */
	private static SSLContext init(SSLContext context) {
		try {
			KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
			ks.load(null); // We don't need the KeyStore instance to come from a file.
			InputStream is = new ByteArrayInputStream(SERVER_CERTIFICATE.getBytes());
			ks.setCertificateEntry("keysqlCA", CertificateFactory.getInstance("X.509").generateCertificate(is));
			//
			TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			tmf.init(ks);
			//
			context.init(null, tmf.getTrustManagers(), null);
		} catch (GeneralSecurityException | IOException e) {
			throw new RuntimeException("Failed to initialize an SSLContext.", e);
		}

		return context;
	}
}
/*
 **********************************************************************************
 * Copyright (C) 2017-present KEYARK, INC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
