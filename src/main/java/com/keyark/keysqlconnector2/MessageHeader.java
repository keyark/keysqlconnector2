/*
 **********************************************************************************
 * Copyright (C) 2017-present KEYARK, INC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
package com.keyark.keysqlconnector2;

import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * @author Konstantin Andreyev kandreyev@keyark.com
 *
 */
public final class MessageHeader implements Constants {
	private final long messageLength;
	private long clientMessageId;
	private int exitCode;
	private StatementType type = StatementType.UNKNOWN;
	private long instanceCount = 0;
	private double elapsedTime = -1.0;
	private int page = 0;
	private int query = 0;
	private MessageFormat messageFormat = null; // keysql/ json
	private String encoding = null;
	private ReplyObj src = null;

	private String messageText = null;

	/**
	 * @param len             - message length in bytes
	 * @param clientMessageId - client message Id
	 */
	MessageHeader(long len, long clientMessageId) {
		this.messageLength = len;
		this.clientMessageId = clientMessageId;
	}

	/**
	 * @param error - error code
	 */
	MessageHeader(int error) {
		this.exitCode = error;
		this.messageLength = 0;
		this.clientMessageId = -1;
	}

	/**
	 * @param headerStr - message header
	 */
	MessageHeader(String headerStr) {
		String[] parts = headerStr.substring(0, headerStr.indexOf('\n')).split(" ");
		if (parts.length > 0) {
			messageLength = Long.parseLong(parts[0]);
		} else {
			messageLength = 0;
		}
		if (parts.length > 1) {
			clientMessageId = Long.parseLong(parts[1]);
		}
		if (parts.length > 2) {
			exitCode = Integer.parseInt(parts[2]);
		}
		for (int i = 3; i < parts.length; i += 2) {
			if (i == parts.length - 1) {
				break;
			}
			String prompt = parts[i];
			String value = parts[i + 1];

			switch (prompt) {
			case "type":
				type = StatementType.fromInteger(Integer.parseInt(value));
				break;
			case "count":
				instanceCount = Long.parseLong(value);
				break;
			case "query":
				query = Integer.parseInt(value);
				break;
			case "page":
				page = Integer.parseInt(value);
				break;
			case "format":
				messageFormat = MessageFormat.valueOf(value);
				break;
			case "encoding":
				encoding = value;
				break;
			case "time":
				elapsedTime = Double.parseDouble(value);
				break;
			case "src":
				src = ReplyObj.valueOf(Integer.parseInt(value));
				break;
			}
		}
	}

	/**
	 * @return .
	 */
	public int getExitCode() {
		return exitCode;
	}

	/**
	 * @return .
	 */
	public long getMessageLength() {
		return messageLength;
	}

	/**
	 * @return .
	 */
	public long getClientMessageId() {
		return clientMessageId;
	}

	/**
	 * @return .
	 */
	public StatementType getStatementType() {
		return type;
	}

	/**
	 * @return .
	 */
	public long getInstanceCount() {
		return instanceCount;
	}

	/**
	 * @param n .
	 */
	void setInstanceCount(long n) {
		instanceCount = n;
	}

	/**
	 * @param page .
	 */
	void setPage(int page) {
		this.page = page;
	}

	/**
	 * @return .
	 */
	public int getPage() {
		return page;
	}

	/**
	 * @param query .
	 */
	public void setQuery(int query) {
		this.query = query;
	}
	/**
	 * @return .
	 */
	public int getQuery() {	return query; }

	/**
	 * @return .
	 */
	public MessageFormat getMessageFormat() {
		return messageFormat;
	}

	/**
	 * @param messageFormat .
	 */
	void setMessageFormat(MessageFormat messageFormat) {
		this.messageFormat = messageFormat;
	}

	/**
	 * @return .
	 */
	public String getEncoding() {
		return encoding;
	}

	/**
	 * @param encoding .
	 */
	void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	/**
	 * @return .
	 */
	public boolean jsonReply() {
		return messageFormat == MessageFormat.json;
	}

	/**
	 * @param text .
	 */
	void setMessageText(String text) {
		messageText = text;
	}

	/**
	 * @return .
	 */
	public String getMessageText() {
		return messageText;
	}

	/**
	 * @return .
	 */
	public ReplyObj getSrc() {
		return src;
	}

	/**
	 * @param time .
	 */
	void setElapsedTime(double time) {
		elapsedTime = time;
	}

	/**
	 * @return .
	 */
	public double getElapsedTime() {
		return elapsedTime;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(messageLength + " " + clientMessageId + " " + exitCode);
		if (type != StatementType.UNKNOWN) {
			sb.append(" type ").append(type.getValue());
		}
		if (instanceCount >= 0) {
			sb.append(" count ").append(instanceCount);
		}
		if (page > 0) {
			sb.append(" page ").append(page);
		}
		if (messageFormat != null) {
			sb.append(" format ").append(messageFormat);
		}
		if (encoding != null && !encoding.isEmpty()) {
			sb.append(" encoding ").append(encoding);
		}
		if (src != null) {
			sb.append(" src ").append(src);
		}
		return sb.append('\n').toString();
	}

	/**
	 * @return .
	 */
	public String getResultLine() {
		if (EXIT_CODE_SUCCESS != exitCode) {
			return "";
		}
		DecimalFormat df = new DecimalFormat("#.##");
		df.setRoundingMode(RoundingMode.HALF_DOWN);
		String elapsed = "(" + df.format(elapsedTime) + " sec)";
		switch (type) {
		case SELECT:
			return instanceCount + " instance" + (instanceCount == 1 ? "" : "s") + " in result store " + elapsed;
		case DELETE:
		case INSERT:
		case UPDATE:
			return "KeySQL query OK, " + instanceCount + " instance" + (instanceCount == 1 ? "" : "s") + " affected " + elapsed;

		case CREATE:
		case ALTER: // * VP 8/1/2019
		case DROP:
			return "KeySQL query OK. " + elapsed;

		default:
			return "";

		}
	}
}
/*
 **********************************************************************************
 * Copyright (C) 2017-present KEYARK, INC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
