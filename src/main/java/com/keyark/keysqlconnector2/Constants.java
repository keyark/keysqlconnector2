/*
 **********************************************************************************
 * Copyright (C) 2017-present KEYARK, INC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
package com.keyark.keysqlconnector2;

/**
 * @author Konstantin Andreyev kandreyev@keyark.com
 *
 */
public interface Constants {
	/**
	 *
	 */
	int EXIT_CODE_SUCCESS = 0;
	/**
	 *
	 */
	int EXIT_CODE_FATAL_ERROR = 500;
	/**
	 *
	 */
	int CONNECTION_TIMEOUT = 7200 * 1000;//7200 seconds

	/**
	 *
	 */
	String SERVER_CERTIFICATE = """
			-----BEGIN CERTIFICATE-----
			MIIDLzCCAhcCFCcDyr6f/Y8RgPil5L3UI+n6TUfvMA0GCSqGSIb3DQEBCwUAMFMx
			CzAJBgNVBAYTAkZSMQ0wCwYDVQQKDARrcmtyMSEwHwYDVQQLDBhEb21haW4gQ29u
			dHJvbCBWYWxpZGF0ZWQxEjAQBgNVBAMMCSoua3Jrci5pbzAgFw0yMDA2MTEyMjA1
			MjVaGA8yMTIwMDUxODIyMDUyNVowUzELMAkGA1UEBhMCRlIxDTALBgNVBAoMBGty
			a3IxITAfBgNVBAsMGERvbWFpbiBDb250cm9sIFZhbGlkYXRlZDESMBAGA1UEAwwJ
			Ki5rcmtyLmlvMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA6av3SotQ
			U5EwbIHGljgRbrSXLZRrWqnV8F4uEy5CBJEeUQk8TujFmHVk+VW1rSYt5BpasdnD
			mDqhFlMzu8wwCehg2EAm5s+dVbVmC7HrFVx4dDRV9TL3laccqmxSSdZmpKTabHp0
			7AbDozWLPtNGNZGnW3YlOLCg1pRXEMZZTBbiB72FzAnh7NMVgGVT4lEdZEhhSgLs
			OIbtUUCAZFBk+crRleNsbKKfCwOfvl/7luLBbvNn/Hqm2nHo7iaVtLFBIe4GXwcb
			Th3v5ZgZ4l8mvEVJ2j9BMqSwZLFzU0SV6fRvEvqxbjMCHze1XI6yoNfi6oFGrmhB
			piemFo76sGvw5QIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQAkOR+lNtTWBB43xewm
			R3tifbjjmlPZtPYS3LceJT4IM1Ia1gMz2LEnN0Za9hTP/nPf9Eyqry6uqJ1g/JhB
			KTR3X1BjG6uyEwdcNVOyMp3dEZUMhXoPfW75n/r113T/0gzznU9j7U0GHQU9eJkq
			sYD2roJg3Wb3Mg0hTHbuXg7imF0cW/NPpiR8ri14yM7jVsfE6FMMLM4gnDPsSomo
			aPN8cTlO+8yZibEZQIbdteoTx2ZqkKTbT8/Jzp4nRcgTTpIfBqh+Z0G5M5AKUi3E
			rSzBnSOcK5B58DPDC6O2aNXu2rnJsmcO1mTyACmkDYOi5gBqBkU0+mJCSxLU69g8
			bJEw
			-----END CERTIFICATE-----""";
	/**
	 *
	 */
	String SERVER_REPLY_OK = "OK\n";
	/**
	 *
	 */
	String SERVER_REPLY_PONG = "PONG\n";
	/**
	 *
	 */
	String SERVER_REPLY_NOT_CONNECTED = "FAILED NOT CONNECTED\n";
	/**
	 *
	 */
	String SERVER_REPLY_NOT_REQUEST = "FAILED NOT REQUEST\n";
	/**
	 *
	 */
	String SERVER_REPLY_WRONG_USER = "FAILED WRONG USER/PASSWORD\n";
	/**
	 *
	 */
	String SERVER_REPLY_BAD_REQUEST = "FAILED BAD REQUEST\n";
	/**
	 *
	 */
	String SERVER_REPLY_NOT_REPLIED = "FAILED NOT REPLIED\n";
	/**
	 *
	 */
	String[] SUCCESS_SERVER_REPLIES = { SERVER_REPLY_OK, SERVER_REPLY_PONG };
	/**
	 *
	 */
	String[] SERVER_REPLIES = { SERVER_REPLY_OK, SERVER_REPLY_PONG, SERVER_REPLY_NOT_CONNECTED, SERVER_REPLY_NOT_REQUEST,
			SERVER_REPLY_WRONG_USER, SERVER_REPLY_BAD_REQUEST, SERVER_REPLY_NOT_REPLIED };
}
/*
 **********************************************************************************
 * Copyright (C) 2017-present KEYARK, INC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
