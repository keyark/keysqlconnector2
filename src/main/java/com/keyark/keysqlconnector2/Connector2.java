/*
 **********************************************************************************
 * Copyright (C) 2017-present KEYARK, INC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
package com.keyark.keysqlconnector2;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.locks.ReentrantLock;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.keyark.KSException;
import com.keyark.UserIsNotAuthenticatedException;

/**
 * @author Konstantin Andreyev kandreyev@keyark.com
 *
 */
public final class Connector2 implements Constants, KSConnector {
	private static final Logger logger = LoggerFactory.getLogger(Connector2.class);
	private final ReentrantLock lock = new ReentrantLock();
	private final String host;
	private final int port;
	private final boolean tls;
	private final int connectionTimeout;
	private final Socket socket;
	//
	private boolean authenticated = false;

	/**
	 * @param host              - hostname or host address
	 * @param port              - port number
	 * @param tls               - use TLS or not
	 * @param connectionTimeout - the timeout value to be used in milliseconds.
	 * @throws NoSuchAlgorithmException - if a particular cryptographic algorithm is requested but is not available in the
	 *                                  environment.
	 * @throws IOException              - if the sslsocket cannot be created
	 */
	public Connector2(String host, int port, boolean tls, int connectionTimeout) throws NoSuchAlgorithmException, IOException {
		this.host = host;
		this.port = port;
		this.tls = tls;
		this.connectionTimeout = connectionTimeout;
		if (tls) {
			SSLContext context = KeysqlSSLContext.getInstance("TLS");
			SSLSocketFactory sslSocketFactory = context.getSocketFactory();
			this.socket = sslSocketFactory.createSocket();
		} else {
			this.socket = new Socket();
		}
		socket.setSoTimeout(connectionTimeout);
		socket.setReuseAddress(true);
	}

	/**
	 * @param host - hostname or host address
	 * @param port - port number
	 * @param tls  - use TLS or not
	 * @throws NoSuchAlgorithmException - if a particular cryptographic algorithm is requested but is not available in the
	 *                                  environment.
	 * @throws IOException              - if the sslsocket cannot be created
	 */
	public Connector2(String host, int port, boolean tls) throws NoSuchAlgorithmException, IOException {
		this(host, port, tls, CONNECTION_TIMEOUT);
	}

	private void connect() throws IOException {
		socket.connect(new InetSocketAddress(host, port), connectionTimeout);
		logger.info("Connection established to " + socket.getInetAddress().toString() + ":" + socket.getPort());
		printSocketInfo();
	}

	@Override
	@SuppressWarnings("resource")
	public boolean ping() throws IOException {
		lock.lock();
		try {
			if (!socket.isConnected()) {
				connect();
			}
			//
			OutputStream out = socket.getOutputStream();
			out.write("PING\n".getBytes());
			//
			MessageHeader responseHeader = readResponseHeader(socket.getInputStream());
			logger.debug("PING response: " + responseHeader.getMessageText());

			return responseHeader.getExitCode() == EXIT_CODE_SUCCESS
					&& SERVER_REPLY_PONG.equals(responseHeader.getMessageText());
		} catch (Throwable t) {
			try {
				socket.close();
			} catch (IOException ignore) {
			}
			throw t;
		} finally {
			lock.unlock();
		}
	}

	@Override
	public String bindUser(String userName, String pwd) throws IOException {
		return bindUser(userName, pwd, null);
	}

	@SuppressWarnings("resource")
	@Override
	public String bindUser(String userName, String pwd, String schema) throws IOException {
		lock.lock();
		OutputStream out = null;
		try {
			if (!socket.isConnected()) {
				connect();
			}
			//
			StringBuilder sb = new StringBuilder();
			sb.append("CONNECT --user ").append(userName).append(" --password ").append(pwd);
			if (schema != null && schema.length() > 0) {
				sb.append(" --schema ").append(schema);
			}
			sb.append("\n");
			out = socket.getOutputStream();
			out.write(sb.toString().getBytes());
			out.flush();
			//
			InputStream in = socket.getInputStream();
			MessageHeader responseHeader = readResponseHeader(socket.getInputStream());
			authenticated = responseHeader.getExitCode() == EXIT_CODE_SUCCESS
					&& SERVER_REPLY_OK.equals(responseHeader.getMessageText());
			if (authenticated) {
				logger.info("SUCCESSFUL LOGIN: " + userName);
			} else {
				logger.error("INVALID CREDENTIALS: " + userName + " - " + responseHeader.getMessageText());
			}
			//
			return responseHeader.getMessageText();
		} catch (Throwable t) {
			try {
				socket.close();
			} catch (IOException ignore) {
			}
			throw t;
		} finally {
			lock.unlock();
		}
	}

	@SuppressWarnings("resource")
	@Override
	public String changePassword(String userName, String oldPwd, String newPwd) throws IOException {
		lock.lock();
		OutputStream out = null;
		try {
			if (!socket.isConnected()) {
				connect();
			}
			//
			StringBuilder sb = new StringBuilder();
			sb.append("CHANGE-PASSWORD --user ").append(userName).append(" --old-password ").append(oldPwd);
			sb.append(" --new-password ").append(newPwd).append("\n");
			//
			out = socket.getOutputStream();
			out.write(sb.toString().getBytes());
			out.flush();
			//
			InputStream in = socket.getInputStream();
			MessageHeader responseHeader = readResponseHeader(in);
			Boolean ok = responseHeader.getExitCode() == EXIT_CODE_SUCCESS
					&& SERVER_REPLY_OK.equals(responseHeader.getMessageText());
			if (ok) {
				logger.info("SUCCESSFULLY CHANGED PASSWORD BY: " + userName);
			} else {
				logger.error("FAILED CHANGE-PASSWORD - WRONG USER/PASSWORD OR ACCOUNT LOCKED: " + userName + ":" + oldPwd);
			}
			//
			return responseHeader.getMessageText();
		} catch (Throwable t) {
			try {
				socket.close();
			} catch (IOException ignore) {
			}
			throw t;
		} finally {
			lock.unlock();
		}
	}
	@SuppressWarnings("resource")
	@Override
	public String logout() throws IOException {
		lock.lock();
		OutputStream out = null;
		try {
			if (!authenticated) {
				socket.close();
				throw new UserIsNotAuthenticatedException();
			}
			out = socket.getOutputStream();
			out.write("DISCONNECT\n".getBytes());
			out.flush();
			MessageHeader responseHeader = readResponseHeader(socket.getInputStream());
			boolean ok = responseHeader.getExitCode() == EXIT_CODE_SUCCESS
					&& SERVER_REPLY_OK.equals(responseHeader.getMessageText());
			if (ok) {
				authenticated = false;
				logger.debug("SUCCESSFUL LOGOUT");
			}
			else {
				logger.debug("DISCONNECT response: " + responseHeader.getMessageText());
			}
			//
			return responseHeader.getMessageText();
		} catch (Throwable t) {
			try {
				socket.close();
			} catch (IOException ignore) {
			}
			throw t;
		} finally {
			lock.unlock();
		}
	}

	@Override
	@SuppressWarnings("resource")
	public String reconnect(String dbSchema) throws IOException {
		lock.lock();
		OutputStream out = null;
		try {
			if (!authenticated) {
				socket.close();
				throw new UserIsNotAuthenticatedException();
			}
			logger.info("Trying to RECONNECT to schema " + dbSchema);
			StringBuilder sb = new StringBuilder();
			sb.append("RECONNECT --schema ").append(dbSchema).append("\n");
			out = socket.getOutputStream();
			out.write(sb.toString().getBytes());
			out.flush();
			MessageHeader responseHeader = readResponseHeader(socket.getInputStream());
			logger.info("RECONNECTED to schema " + dbSchema + " response: " + responseHeader.getMessageText());
			//
			return responseHeader.getMessageText();
		} catch (Throwable t) {
			try {
				socket.close();
			} catch (IOException ignore) {
			}
			throw t;
		} finally {
			lock.unlock();
		}

	}

	@Override
	@SuppressWarnings("resource")
	public ReplyMessage submit(final String parQuery, final MessageFormat messageFormat) throws IOException {
		lock.lock();
		OutputStream out = null;
		try {
			if (!authenticated) {
				socket.close();
				throw new UserIsNotAuthenticatedException();
			}
			byte[] query = parQuery.getBytes(StandardCharsets.UTF_8);
			//
			MessageHeader header = new MessageHeader(query.length, 1);
			header.setMessageFormat(messageFormat);
			//
			String req = ("SUBMIT " + header.toString());
			logger.debug("REQUEST: " + req + requestSubstring(parQuery));
			//
			out = socket.getOutputStream();
			out.write(req.getBytes());
			out.write(query);
			//
			return new ReplyMessage(socket.getInputStream());
		} catch (Throwable t) {
			try {
				socket.close();
			} catch (IOException ignore) {
			}
			throw t;
		} finally {
			if (out != null) {
				try {
					out.flush();
				} catch (@SuppressWarnings("unused") IOException ignore) {
				}
			}
			lock.unlock();
		}
	}

	/**
	 * @param request
	 * @return
	 */
	private String requestSubstring(String request) {
		//
		return request != null ? (request.length() < 120 ? request : request.substring(0, 120) + ".....") : null;
	}

	@Override
	@SuppressWarnings("resource")
	public void submit(final String parQuery, final MessageFormat messageFormat, OutputStream output) throws IOException {
		lock.lock();
		OutputStream out = null;
		try {
			if (!authenticated) {
				socket.close();
				throw new UserIsNotAuthenticatedException();
			}
			byte[] query = parQuery.getBytes(StandardCharsets.UTF_8);
			//
			MessageHeader header = new MessageHeader(query.length, 2000);
			header.setMessageFormat(messageFormat);
			//
			String req = ("SUBMIT " + header.toString());
			logger.debug("REQUEST: " + req + requestSubstring(parQuery));
			//
			out = socket.getOutputStream();
			out.write(req.getBytes());
			out.write(query);
			//
			copyResponse(socket.getInputStream(), output);
		} catch (Throwable t) {
			try {
				socket.close();
			} catch (IOException ignore) {
			}
			throw t;
		} finally {
			if (out != null) {
				try {
					out.flush();
				} catch (@SuppressWarnings("unused") IOException ignore) {
				}
			}
			lock.unlock();
		}
	}

	private void copyResponse(InputStream in, OutputStream output) {
		String firstLine = null;
		boolean aborted = false;
		//		boolean abortResponseRead = false;
		try {
			int pageNum = 1;
			int queryNum = 1;
			while (pageNum > 0 || queryNum > 0) {
				firstLine = readLine(in);
				logger.debug("page header: " + firstLine);
				MessageHeader header = new MessageHeader(firstLine);
				byte[] pageData = readPageData(in, header.getMessageLength());
				int separator = in.read();//read \n page separator which is not included in page data
				if (separator != '\n') {
					throw new IOException("page separator is not found at the end of the page. Page header is: " + firstLine);
				}
				pageNum = header.getPage();
				queryNum = header.getQuery();
				//
				if (!aborted) {
					try {
						output.write(pageData);
					} catch (IOException e) {
						try {
							aborted = true;
							logger.info("Aborting current query. Root cause: " + e.toString());
							abort();
						} catch (@SuppressWarnings("unused") IOException ignore) {
						}
					}
				}
			}
		} catch (Throwable t) {
			logger.error(t.toString(), t);
			throw new KSException(t);
		}
		//
	}

	/**
	 * @param in
	 * @param messageLength
	 * @return
	 * @throws IOException
	 */
	private byte[] readPageData(InputStream in, long messageLength) throws IOException {
		byte[] pageData = new byte[(int) messageLength];
		int off = 0;
		int len = pageData.length;
		int numRead = -1;
		while ((numRead = in.read(pageData, off, len)) != -1 && len > 0) {
			off += numRead;
			len -= numRead;
		}
		//
		return pageData;
	}

	private String readLine(final InputStream in) throws IOException {
		StringBuffer sb = new StringBuffer();
		int ch = -1;
		while ((ch = in.read()) != -1) {
			sb.append((char) ch);
			if (ch == 10) {
				break;
			}
		}
		//
		return sb.toString();
	}

	private MessageHeader readResponseHeader(final InputStream in) throws IOException {
		String line1 = readLine(in);
		MessageHeader header = new MessageHeader(line1);
		if (header.getMessageLength() != 0) {
			byte[] bytes = in.readNBytes((int)header.getMessageLength() + 1);
			header.setMessageText(new String(bytes, StandardCharsets.UTF_8));
		}
		return header;
	}

	private void printSocketInfo() {
		logger.info("Socket class: " + socket.getClass());
		logger.info("   Remote address = " + socket.getInetAddress().toString() + ":" + socket.getPort());
		logger.info("   Local socket address = " + socket.getLocalSocketAddress().toString());
		if (tls) {
			logger.info("   Need client authentication = " + ((SSLSocket) socket).getNeedClientAuth());
			SSLSession ss = ((SSLSocket) socket).getSession();
			logger.info("   Cipher suite = " + ss.getCipherSuite());
			logger.info("   Protocol = " + ss.getProtocol());
		}
	}

	@Override
	@SuppressWarnings("resource")
	public void abort() throws IOException {
		OutputStream out = null;
		try {
			if (!authenticated) {
				socket.close();
				throw new UserIsNotAuthenticatedException();
			}
			out = socket.getOutputStream();
			String req = ("ABORT\n");
			logger.debug("REQUEST: " + req);

			out.write(req.getBytes());
			//
		} catch (Throwable t) {
			try {
				socket.close();
			} catch (IOException ignore) {
			}
			throw t;
		} finally {
			logger.debug(lock.toString());
			if (out != null) {
				try {
					out.flush();
				} catch (@SuppressWarnings("unused") IOException ignore) {
				}
			}
		}

	}

	/**
	 * submitAndGetFirstPage Acquires the lock which will be released in getNextPage() when last page reached.
	 *
	 * @param parQuery      - request query
	 * @param messageFormat - message format for response - ksql or json
	 * @return not null ReplyPage
	 *
	 */
	@Override
	@SuppressWarnings("resource")
	public ReplyPage submitAndGetFirstPage(final String parQuery, final MessageFormat messageFormat) {
		lock.lock();
		logger.debug(lock.toString());
		OutputStream out = null;
		String firstLine = null;
		try {
			if (authenticated) {
				byte[] query = parQuery.getBytes(StandardCharsets.UTF_8);
				//
				MessageHeader header = new MessageHeader(query.length, 1);
				header.setMessageFormat(messageFormat);
				//
				String req = ("SUBMIT " + header.toString());
				logger.debug("REQUEST: " + req + requestSubstring(parQuery));
				//
				out = socket.getOutputStream();
				out.write(req.getBytes());
				out.write(query);
			}
			//
			return getNextPage();
			//
		} catch (Throwable t) {
			logger.error("FATAL ", t);
			try {
				socket.close();
			} catch (IOException ignore) {
			}
			if (lock.isHeldByCurrentThread()) {
				lock.unlock();
				logger.error(lock.toString());
			}
			return new ReplyPage(new MessageHeader(EXIT_CODE_FATAL_ERROR), firstLine);
		} finally {
			if (out != null) {
				try {
					out.flush();
				} catch (@SuppressWarnings("unused") IOException ignore) {
				}
			}
		}
	}

	/**
	 *
	 * @return next Page
	 */
	@Override
	@SuppressWarnings({ "resource", "unused" })
	public ReplyPage getNextPage() {
		int pageNum = 0;
		int queryNum = 0;
		String firstLine = null;
		try {
			if (!authenticated) {
				socket.close();
				throw new UserIsNotAuthenticatedException();
			}
			InputStream in = socket.getInputStream();
			firstLine = readLine(in);
			logger.debug("page header: " + firstLine);
			MessageHeader responseHeader = new MessageHeader(firstLine);
			byte[] pageData = new byte[(int) responseHeader.getMessageLength()];
			int off = 0;
			int len = pageData.length;
			int numRead = -1;
			while ((numRead = in.read(pageData, off, len)) != -1 && len > 0) {
				off += numRead;
				len -= numRead;
			}
			int separator = in.read();//read \n page separator which is not included in page data
			if (separator != '\n') {
				throw new IOException("page separator is not found at the end of the page. Page header is: " + firstLine);
			}
			ReplyPage page = new ReplyPage(responseHeader, new String(pageData, StandardCharsets.UTF_8));
			pageNum = page.getHeader().getPage();
			queryNum = page.getHeader().getQuery();
			//
			return page;
		} catch (Throwable t) {
			try {
				socket.close();
			} catch (IOException ignore) {
			}
			pageNum = 0;
			logger.error("FATAL ", t);
			return new ReplyPage(new MessageHeader(EXIT_CODE_FATAL_ERROR), t.getMessage());
		} finally {
			if (pageNum == 0 && queryNum == 0) {
				lock.unlock();
				logger.debug(lock.toString());
			}
		}
	}

	/**
	 *
	 */
	@Override
	public void gracefullyClose() {
		lock.lock();
		logger.debug("close()::" + lock.toString());
		try {
			close();
		} finally {
			lock.unlock();
			logger.debug("close()::" + lock.toString());
		}
	}

	@Override
	public void close() {
		try {
			if (socket != null && !socket.isClosed()) {
				String addr = String.valueOf(socket.getInetAddress()) + ":" + socket.getPort();
				try {
					socket.close();
					logger.info("socket to " + addr + " has been closed");
				} catch (@SuppressWarnings("unused") IOException ignore) {
				}
			}
		} finally {
			authenticated = false;
			while (lock.getHoldCount() > 0) {
				lock.unlock();
			}
		}
	}

	@Override
	public boolean isClosed() {
		return socket == null ? true : socket.isClosed();
	}

	@Override
	public boolean isAuthenticated() {
		lock.lock();
		try {
			return authenticated;
		} finally {
			lock.unlock();
		}
	}

	//	@Override
	//	protected void finalize() throws Throwable {
	//		close();
	//	}

}

/*
 **********************************************************************************
 * Copyright (C) 2017-present KEYARK, INC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
