/*
 **********************************************************************************
 * Copyright (C) 2017-present KEYARK, INC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
package com.keyark.keysqlconnector2;

/**
 * @author Konstantin Andreyev kandreyev@keyark.com
 *
 */
public enum StatementType {
	/**
	 *
	 */
	UNKNOWN(0),
	/**
	 *
	 */
	CREATE(1),
	/**
	 *
	 */
	DROP(2),
	/**
	 *
	 */
	SHOW(3),
	/**
	 *
	 */
	ALTER(4),
	/**
	 *
	 */
	INSERT(5),
	/**
	 *
	 */
	DELETE(6),
	/**
	 *
	 */
	UPDATE(7),
	/**
	 *
	 */
	SELECT(8),
	/**
	 *
	 */
	GRANT(9),
	/**
	 *
	 */
	REVOKE(10),
	/**
	 *
	 */
	GENERIC(11),
	/**
	 *
 	 */
	TRUNCATE(12),
	/**
	 *
 	 */
	DESCRIBE(13),
	/**
	 *
 	 */
	BEGIN(14),
	/**
	 *
 	 */
	COMMIT(15),
	/**
	 *
	 */
	ROLLBACK(16),
	/**
	 *
	 */
	EMPTY(17);

	private final int value;

	StatementType(int value) {
		this.value = value;
	}

	/**
	 * @return .
	 */
	public int getValue() {
		return value;
	}

	/**
	 * @param value .
	 * @return .
	 */
	public static StatementType fromInteger(int value) {
		switch (value) {
		case 0:
			return StatementType.UNKNOWN;
		case 1:
			return StatementType.CREATE;
		case 2:
			return StatementType.DROP;
		case 3:
			return StatementType.SHOW;
		case 4:
			return StatementType.ALTER;
		case 5:
			return StatementType.INSERT;
		case 6:
			return StatementType.DELETE;
		case 7:
			return StatementType.UPDATE;
		case 8:
			return StatementType.SELECT;
		case 9:
			return StatementType.GRANT;
		case 10:
			return StatementType.REVOKE;
		case 11:
			return StatementType.GENERIC;
		case 12:
			return StatementType.TRUNCATE;
		case 13:
			return StatementType.DESCRIBE;
		case 14:
			return StatementType.BEGIN;
		case 15:
			return StatementType.COMMIT;
		case 16:
			return StatementType.ROLLBACK;
		case 17:
			return StatementType.EMPTY;
		}
		throw new IllegalArgumentException("Unknown StatementType value=" + value);
	}
	/*
	* 2023-05-06
	* common.hpp
	*
 	enum class statement_type_t {
	unknown_st = 0,
	create_st = 1,
	drop_st = 2,
	show_st = 3,
	alter_st = 4,
	insert_st = 5,
	delete_st = 6,
	update_st = 7,
	select_st = 8,
	grant_st = 9,
	revoke_st = 10,
	generic_st = 11,
	truncate_st = 12,
	describe_st = 13,
	begin_st = 14,
	commit_st = 15,
	rollback_st = 16,
	empty_st = 17
	};
	 */
}
/*
 **********************************************************************************
 * Copyright (C) 2017-present KEYARK, INC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */

