/*
 **********************************************************************************
 * Copyright (C) 2017-present KEYARK, INC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
package com.keyark;

import java.io.Serial;

/**
 * @author Konstantin Andreyev kandreyev@keyark.com
 *
 */
public class KSException extends RuntimeException {
	@Serial
	private static final long serialVersionUID = 7718828512143293558L;

	/**
	 *
	 */
	private final int code;

	/**
	 * @param message .
	 * @param cause .
	 * @param code .
	 */
	public KSException(String message, Throwable cause, int code) {
		super(message, cause);
		this.code = code;
	}

	/**
	 * @param message .
	 * @param code .
	 */
	public KSException(String message, int code) {
		super(message);
		this.code = code;
	}

	/**
	 * @param code .
	 */
	public KSException(int code) {
		this.code = code;
	}

	/**
	 * @param message .
	 */
	public KSException(String message) {
		this(message, 500);
	}

	/**
	 * @param cause .
	 * @param code .
	 */
	public KSException(Throwable cause, int code) {
		super(cause);
		this.code = code;
	}

	/**
	 * @param cause .
	 */
	public KSException(Throwable cause) {
		this(cause, 500);
	}

	/**
	 * @return .
	 */
	public int getCode() {
		return this.code;
	}
}
/*
 **********************************************************************************
 * Copyright (C) 2017-present KEYARK, INC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
