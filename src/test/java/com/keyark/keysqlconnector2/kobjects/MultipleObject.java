/*
 **********************************************************************************
 * Copyright (C) 2017-2020 by KEYARK, INC. - All Rights Reserved.
 * All information contained herein is proprietary and confidential to KEYARK, INC.
 * Any use, reproduction, or disclosure without the written
 * permission of KEYARK, INC. is prohibited.
 * Written by Andrey Belyaev <abelyaev@keyark.com>
 **********************************************************************************
*/
package com.keyark.keysqlconnector2.kobjects;

import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

public class MultipleObject implements KeySQLObject {
	private static final String ARRAY_IDX_NAME = "_ARRAY_IDX_NUM";
	private static final Integer MAX_ARRAY_LENGTHS = 20;
	private final String name;
	private final KeySQLObject ref;

	/**
	 * @param name
	 * @param ref
	 */
	public MultipleObject(String name, KeySQLObject ref) {
		super();
		this.name = name;
		this.ref = ref;
	}

	@Override
	public ObjectType getType() {
		return ObjectType.Multiple;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (other == null) {
			return false;
		}
		if (!(other instanceof MultipleObject)) {
			return false;
		}
		MultipleObject o = (MultipleObject) other;
		return name.equals(o.name) && ref.equals(o);
	}

	@Override
	public void createStatements(Set<String> processed, List<String> statements) {
		if (processed.contains(name)) {
			return;
		}
		processed.add(name);
		ref.createStatements(processed, statements);
		statements.add(name + " {" + ref.getName() + " MULTIPLE}");
	}

	@Override
	public String getName() {
		return name;
	}

	public KeySQLObject getRef() {
		return ref;
	}

	@Override
	public String getKInstance(RandomDataProvider randomProvider) {
		String instance = name + ":NULL";
		if (!randomProvider.isNull()) {
			int arrayLength = 1 + (int) Math.round(Math.random() * (MAX_ARRAY_LENGTHS - 1));
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < arrayLength; i++) {
				if (sb.length() > 0) {
					sb.append(',');
				}
				String str = null;
				if (ref instanceof ComposedObject) {
					StringBuilder sb2 = new StringBuilder();
					for (Entry<String, KeySQLObject> entry : ((ComposedObject) ref).getChilds().entrySet()) {
						if (sb2.length() > 0) {
							sb2.append(',');
						}
						if (ARRAY_IDX_NAME.equals(entry.getValue().getName())) {
							sb2.append(ARRAY_IDX_NAME).append(":").append(i);
						} else {
							sb2.append(entry.getValue().getKInstance(randomProvider));
						}
					}
					str = sb2.insert(0, ref.getName() + ":{").append("}").toString();
					//
				} else {
					str = ref.getKInstance(randomProvider);
				}
				if (str != null) {
					sb.append(str);
				}
			}
			if (sb.length() > 0) {
				sb.insert(0, name + ":{");
				sb.append("}");
				instance = sb.toString();
			}
		}
		//
		return instance;
	}

}
/*
 **********************************************************************************
 * Copyright (C) 2017-2020 by KEYARK, INC. - All Rights Reserved.
 * All information contained herein is proprietary and confidential to KEYARK,
 * INC.
 * Any use, reproduction, or disclosure without the written
 * permission of KEYARK, INC. is prohibited.
 * Written by Andrey Belyaev <abelyaev@keyark.com>
 **********************************************************************************
 */
