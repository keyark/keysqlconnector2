/*
 **********************************************************************************
 * Copyright (C) 2017-2020 by KEYARK, INC. - All Rights Reserved.
 * All information contained herein is proprietary and confidential to KEYARK, INC.
 * Any use, reproduction, or disclosure without the written
 * permission of KEYARK, INC. is prohibited.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
package com.keyark.keysqlconnector2.kobjects;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.keyark.keysqlconnector2.*;
import org.slf4j.Logger;

/**
 * @author Konstantin Andreyev kandreyev@keyark.com
 *
 */
public class KeySqlRequestHelper {
    private static final String SELECT_INDENT = "  ";

    public String submitRequest(String request, KSConnector connector, Logger logger) throws IOException {
        String[] queries = createScript(request);
        StringBuilder sb = new StringBuilder();
        ReplyMessage[] replies = submitRequests(queries, connector, logger);
        for (ReplyMessage reply : replies) {
            sb.append(getFormatedReply(reply, reply.getReply(), true));/////////////////////////////
        }
        //
        return sb.toString();
    }

    public ReplyMessage[] submitRequests(String[] queries, KSConnector connector, Logger logger) throws IOException {
        Collection<ReplyMessage> replies = new ArrayList<ReplyMessage>();
        for (final String query : queries) {
            if (query.length() > 100 && !logger.isDebugEnabled()) {
                logger.info("Request(partual): " + query.substring(0, 100) + "....");
            } else {
                logger.info("Request: " + query);
            }
            //
            ReplyMessage reply = connector.submit(query, MessageFormat.keysql);
            //
            if (reply.isSuccess()) {
                replies.add(reply);
                logger.info("Get response " + String.valueOf(reply.getReply()).length() + " characters long.");
                logger.debug("Reply: " + String.valueOf(reply.getReply()));
            } else {
                logger.error(reply.getReply());
                throw new IOException(reply.getReply());
            }
        }
        //
        return replies.toArray(new ReplyMessage[replies.size()]);
    }

    private String getFormatedReply(ReplyMessage replyMessage, String reply, boolean addResultLine) {
        String fmtReply = (replyMessage.getStatementType() == StatementType.SELECT)
                ? formatSelectReply(reply, SELECT_INDENT, replyMessage.isJsonReply())
                : reply;
        if (addResultLine) {
            fmtReply += getResultLine(replyMessage) + "\n";
        }
        //
        return fmtReply;
    }

    private String getResultLine(ReplyMessage reply) {
        if (!reply.isSuccess()) {
            return "";
        }
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.HALF_DOWN);
        String elapsed = "(" + df.format(reply.getElapsedTime()) + " sec)";
        switch (reply.getStatementType()) {
        case SELECT:
            long instanceCount = reply.getTotalInstanceCount();
            return instanceCount + " instance" + (instanceCount == 1 ? "" : "s") + " in result store " + elapsed;
        case DELETE:
        case INSERT:
        case UPDATE:
            instanceCount = reply.getTotalInstanceCount();
            return "KeySQL query OK, " + instanceCount + " instance" + (instanceCount == 1 ? "" : "s") + " affected " + elapsed;
        case CREATE:
        case ALTER: // * VP 8/1/2019
        case DROP:
            return "KeySQL query OK. " + elapsed;

        default:
            return "";

        }
    }

    public String[] getCurlyBracketTokens(final String inputString, int offset) throws IOException {
        Collection<String> tokens = new ArrayList<>();
        Reader stringReader = null;
        try {
            stringReader = new BufferedReader(new StringReader(inputString.substring(offset)));
            String token = null;
            while ((token = readCurlyBracketToken(stringReader)) != null) {
                tokens.add(token);
            }
        } finally {
            if (stringReader != null) {
                try {
                    stringReader.close();
                } catch (IOException ignore) {
                }
            }
        }
        //
        return tokens.toArray(new String[tokens.size()]);
    }

    public String readCurlyBracketToken(final Reader r) throws IOException {
        return readCurlyBracketTokenAndWriteOut(r, null);
    }

    public String readCurlyBracketTokenAndWriteOut(final Reader rd, Writer wr) throws IOException {
        StringBuilder sb = new StringBuilder();
        int brackets = 0;
        int ch = -1;
        while ((ch = rd.read()) != -1) {
            char c = (char) ch;
            if (wr != null) {
                wr.append(c);
            }
            if (c == '{') {
                ++brackets;
                sb.append(c);
            } else if (c == '}' && --brackets == 0) {
                sb.append(c);
                break;
            } else if (brackets > 0) {
                sb.append(c);
            }
            //
        }
        return sb.length() == 0 ? null : sb.toString();
    }

    public String makeErrorMessage(String message) {
        String prefix = "";
        String errPref = "Error";
        int errLen = errPref.length();
        if (message != null && message.length() < errLen) {
            prefix = errPref + ": ";
        } else if (!message.substring(0, errLen).equalsIgnoreCase(errPref)) {
            prefix = errPref + ": ";
        }
        //
        return prefix + message;
    }

    public String[] createScript(String in) {
        final List<String> out = new ArrayList<>();
        final StringBuilder buffer = new StringBuilder();
        Boolean strValue = false, comment = false;
        char prev = 0;
        for (int i = 0; i < in.length(); ++i) {
            char c = in.charAt(i);
            if (c == ';' && !comment && !strValue) {
                String st = buffer.toString().trim();
                if (!st.isEmpty()) {
                    out.add(st);
                }
                buffer.setLength(0);
                prev = 0;
                continue;
            }
            if (c == '\n' && !strValue) {
                comment = false;
                c = ' ';
            } else if (!strValue && !comment && c == '-' && prev == '-') {
                comment = true;
                buffer.deleteCharAt(buffer.length() - 1);
            } else if (!comment && c == '\'') {
                strValue = !strValue;
            }
            if (!comment) {
                buffer.append(c);
            }
            prev = c;
        }
        if (buffer.length() > 0) {
            String st = buffer.toString().trim();
            if (!st.isEmpty()) {
                out.add(st);
            }
        }
        return out.toArray(new String[out.size()]);
    }

    public String formatSelectReply(String reply, String indent, boolean jsonReply) {
        final StringBuilder indents = new StringBuilder();
        final StringBuilder r = new StringBuilder();
        boolean val = false;
        if (jsonReply) {
            indents.append(indent);
            r.append('[').append('\n').append(indents);
        }
        int count = 0;
        for (int i = 0; i < reply.length(); ++i) {
            char c = reply.charAt(i);
            if (val) {
                r.append(c);
                if (c == '\'') {
                    val = false;
                }
                continue;
            }
            switch (c) {
            case ':':
                r.append(' ').append(c).append(' ');
                break;
            case '{':
            case '[':
                ++count;
                indents.append(indent);
                r.append(c).append('\n').append(indents);
                break;
            case ',':
                //				if (i != 0)
                r.append(c);

                if (count > 0) {
                    r.append('\n').append(indents);
                }
                break;
            case '}':
            case ']':
                --count;
                indents.setLength(indents.length() - indent.length());
                r.append('\n').append(indents).append(c);
                break;
            case '\'':
                r.append(c);
                val = true;
                break;
            default:
                r.append(c);
                break;
            }
        }
        if (jsonReply) {
            r.append("\n]");
        }
        return r.toString();
    }

}
/*
 **********************************************************************************
 * Copyright (C) 2017-2020 by KEYARK, INC. - All Rights Reserved.
 * All information contained herein is proprietary and confidential to KEYARK, INC.
 * Any use, reproduction, or disclosure without the written
 * permission of KEYARK, INC. is prohibited.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
