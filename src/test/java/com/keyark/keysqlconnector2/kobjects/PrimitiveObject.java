/*
 **********************************************************************************
 * Copyright (C) 2017-2020 by KEYARK, INC. - All Rights Reserved.
 * All information contained herein is proprietary and confidential to KEYARK, INC.
 * Any use, reproduction, or disclosure without the written
 * permission of KEYARK, INC. is prohibited.
 * Written by Andrey Belyaev <abelyaev@keyark.com>
 **********************************************************************************
*/
package com.keyark.keysqlconnector2.kobjects;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

public class PrimitiveObject implements KeySQLObject {
	static final String KEYSQL_DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";
	private final String name;
	private final PrimitiveObjectType kType;

	public PrimitiveObjectType getPrimitiveObjectType() {
		return kType;
	}

	/**
	 * @param name
	 * @param kType
	 */
	public PrimitiveObject(String name, PrimitiveObjectType kType) {
		super();
		this.name = name;
		this.kType = kType;
	}

	@Override
	public ObjectType getType() {
		switch (this.kType) {
		case CharType:
			return ObjectType.CharType;
		case NumberType:
			return ObjectType.NumberType;
		case DateType:
			return ObjectType.DateType;
		}
		return ObjectType.Unknown;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (other == null) {
			return false;
		}
		if (!(other instanceof PrimitiveObject)) {
			return false;
		}
		PrimitiveObject o = (PrimitiveObject) other;
		return kType == o.kType && name.equals(o.name);
	}

	@Override
	public void createStatements(Set<String> processed, List<String> statements) {
		if (processed.contains(name)) {
			return;
		}
		processed.add(name);
		statements.add(name + " " + PrimitiveObjectType.getType(kType));
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getKInstance(RandomDataProvider randomProvider) {
		String instance = name + ":NULL";
		if (randomProvider.isNull()) {
			;
		} else if (kType == PrimitiveObjectType.DateType) {
			Date date = new Date(randomProvider.randomDate());
			DateFormat kDateFormat = new SimpleDateFormat(KEYSQL_DATE_PATTERN);
			kDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
			instance = name + ":'" + kDateFormat.format(date) + "'";
		} else if (kType == PrimitiveObjectType.NumberType) {
			String numberStr;
			if (randomProvider.isDouble()) {
				numberStr = Double.toString(randomProvider.randomDouble());
			} else {
				numberStr = Integer.toString(randomProvider.randomInt32());
			}
			instance = name + ":" + numberStr;
		} else if (kType == PrimitiveObjectType.CharType) {
			String charStr;
			if ("_ID_CHAR".equalsIgnoreCase(name) || "ID_CHAR".equalsIgnoreCase(name)) {
				charStr = randomProvider.randomHexString24();
			} else if ("YEAR_STR".equalsIgnoreCase(name)) {
				charStr = randomProvider.randomYearString();
			} else {
				charStr = randomProvider.randomMultiLineString();
			}
			instance = name + ":'" + charStr + "'";
		}
		//
		return instance;
	}

}
/*
 **********************************************************************************
 * Copyright (C) 2017-2020 by KEYARK, INC. - All Rights Reserved.
 * All information contained herein is proprietary and confidential to KEYARK,
 * INC.
 * Any use, reproduction, or disclosure without the written
 * permission of KEYARK, INC. is prohibited.
 * Written by Andrey Belyaev <abelyaev@keyark.com>
 **********************************************************************************
 */
