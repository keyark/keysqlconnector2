/*
 **********************************************************************************
 * Copyright (C) 2017-2020 by KEYARK, INC. - All Rights Reserved.
 * All information contained herein is proprietary and confidential to KEYARK, INC.
 * Any use, reproduction, or disclosure without the written
 * permission of KEYARK, INC. is prohibited.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
*/
package com.keyark.keysqlconnector2.kobjects;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import com.keyark.keysqlconnector2.KSConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.keyark.keysqlconnector2.Connector2;

/**
 * @author Konstantin Andreyev kandreyev@keyark.com
 *
 */
public class KObjectService {
	private final KeySqlRequestHelper requestHelper;
	private static final Logger logger = LoggerFactory.getLogger(KObjectService.class);;

	/**
	 *
	 */
	public KObjectService(KeySqlRequestHelper requestHelper) {
		this.requestHelper = requestHelper;
	}

	public KeySQLObject getKeySQLObjFromCatalog(String catalogName, String kObjectName, KSConnector connector)
			throws IOException, InterruptedException, ExecutionException, TimeoutException {
		//
		KeySQLObject KeySQLObj = null;
		//
		String request = "DESCRIBE KEYOBJECT " + String.valueOf(kObjectName).toUpperCase() + " FROM CATALOG " + catalogName + ";";
		String response = requestHelper.submitRequest(request, connector, logger);
		int ind = response.indexOf(kObjectName);
		if (ind > -1) {
			int offset = ind + kObjectName.length();
			if (response.startsWith("CHAR", offset + 1)) {
				KeySQLObj = new PrimitiveObject(kObjectName, PrimitiveObjectType.CharType);
			} else if (response.startsWith("NUMBER", offset + 1)) {
				KeySQLObj = new PrimitiveObject(kObjectName, PrimitiveObjectType.NumberType);
			} else if (response.startsWith("DATE", offset + 1)) {
				KeySQLObj = new PrimitiveObject(kObjectName, PrimitiveObjectType.DateType);
			} else if (response.startsWith("{", offset)) {
				String[] tokens = requestHelper.getCurlyBracketTokens(response, offset);
				if (tokens.length != 1) {
					throw new IOException("Unable to parse response: " + response + "\nto the following request: " + request);
				} else {
					String kObjectMembers = tokens[0].substring(1, tokens[0].length() - 1);
					int mInd = kObjectMembers.indexOf(" MULTIPLE");
					if (mInd > -1) {
						String refObjName = kObjectMembers.substring(0, mInd);
						KeySQLObject ref = getKeySQLObjFromCatalog(catalogName, refObjName, connector);
						KeySQLObj = new MultipleObject(kObjectName, ref);
					} else {
						Map<String, KeySQLObject> childs = new HashMap<>();
						String[] names = kObjectMembers.split(",");
						for (String name : names) {
							childs.put(name, getKeySQLObjFromCatalog(catalogName, name, connector));
						}
						KeySQLObj = new ComposedObject(kObjectName, childs);
					}
				}
			} else {
				throw new IOException("Unable to parse response: " + response + "\nto the following request: " + request);
			}
			// Keyobject MOVIE {CAST,DIRECTOR,GENRE,NOTES,TITLE,YEAR} in catalog CAT1
			// Keyobject CAST {__CHAR_VALUE MULTIPLE} in catalog CAT1
			// Keyobject DIRECTOR CHAR in catalog CAT1
			// Keyobject GENRE CHAR in catalog CAT1
			// Keyobject NOTES CHAR in catalog CAT1
			// Keyobject TITLE CHAR in catalog CAT1
			// Keyobject YEAR NUMBER in catalog CAT1
		}

		return KeySQLObj;
	}

	/**
	 * @param storeName
	 * @param keySqlInstances
	 * @param connector
	 */
	public void executeInsertStatement(String storeName, Collection<String> keySqlInstances, KSConnector connector) throws IOException {
		StringBuilder sb = new StringBuilder();
		for (String keySqlInstance : keySqlInstances) {
			if (sb.length() > 0) {
				sb.append(",");
			}
			sb.append("{").append(keySqlInstance).append("}");
		}
		String statement = "INSERT INTO " + storeName + " INSTANCES " + sb.toString();
		requestHelper.submitRequest(statement, connector, logger);
	}

}

/*
 **********************************************************************************
 * Copyright (C) 2017-2020 by KEYARK, INC. - All Rights Reserved.
 * All information contained herein is proprietary and confidential to KEYARK, INC.
 * Any use, reproduction, or disclosure without the written
 * permission of KEYARK, INC. is prohibited.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
*/