/*
 **********************************************************************************
 * Copyright (C) 2017-2020 by KEYARK, INC. - All Rights Reserved.
 * All information contained herein is proprietary and confidential to KEYARK, INC.
 * Any use, reproduction, or disclosure without the written
 * permission of KEYARK, INC. is prohibited.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
package com.keyark.keysqlconnector2.kobjects;

import java.util.List;
import java.util.Set;

/**
 * @author Konstantin Andreyev kandreyev@keyark.com
 *
 */
public interface KeySQLObject {

	String getName();

	ObjectType getType();

	void createStatements(Set<String> processed, List<String> statements);

	/**
	 * @param randomProvider
	 * @return
	 */
	String getKInstance(RandomDataProvider randomProvider);
}
/*
 **********************************************************************************
 * Copyright (C) 2017-2020 by KEYARK, INC. - All Rights Reserved.
 * All information contained herein is proprietary and confidential to KEYARK, INC.
 * Any use, reproduction, or disclosure without the written
 * permission of KEYARK, INC. is prohibited.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */