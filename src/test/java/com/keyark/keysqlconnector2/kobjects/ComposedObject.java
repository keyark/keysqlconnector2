/*
 **********************************************************************************
 * Copyright (C) 2017-2020 by KEYARK, INC. - All Rights Reserved.
 * All information contained herein is proprietary and confidential to KEYARK, INC.
 * Any use, reproduction, or disclosure without the written
 * permission of KEYARK, INC. is prohibited.
 * Written by Andrey Belyaev <abelyaev@keyark.com>
 **********************************************************************************
*/
package com.keyark.keysqlconnector2.kobjects;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class ComposedObject implements KeySQLObject {
	private final String name;
	private final Map<String, KeySQLObject> childs;

	public ComposedObject(String name, Map<String, KeySQLObject> childs) {
		this.name = name;
		this.childs = childs;
	}

	@Override
	public final ObjectType getType() {
		return ObjectType.Composed;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (other == null) {
			return false;
		}
		if (!(other instanceof ComposedObject)) {
			return false;
		}
		ComposedObject o = (ComposedObject) other;
		if (!name.equals(o.name) || childs.size() != o.childs.size()) {
			return false;
		}
		for (Map.Entry<String, KeySQLObject> entry : childs.entrySet()) {
			KeySQLObject ko = o.childs.get(entry.getKey());
			if (ko == null || !entry.getValue().equals(ko)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public void createStatements(Set<String> processed, List<String> statements) {
		if (processed.contains(name)) {
			return;
		}
		processed.add(name);
		StringBuilder sb = new StringBuilder();
		for (KeySQLObject obj : childs.values()) {
			obj.createStatements(processed, statements);
			if (sb.length() > 0) {
				sb.append(',');
			}
			sb.append(obj.getName());
		}
		if (sb.length() > 0) {
			statements.add(name + " {" + sb.toString() + "}");
		}
	}

	public Map<String, KeySQLObject> getChilds() {
		return childs;
	}

	@Override
	public String getName() {
		//
		return name;
	}

	@Override
	public String getKInstance(RandomDataProvider randomProvider) {
		String instance = name + ":NULL";
		if (!randomProvider.isNull()) {
			StringBuilder sb = new StringBuilder();
			for (Entry<String, KeySQLObject> entry : childs.entrySet()) {
				if (sb.length() > 0) {
					sb.append(',');
				}
				KeySQLObject child = entry.getValue();
				if (child != null) {
					String str = child.getKInstance(randomProvider);
					if (str != null) {
						sb.append(str);
					}
				}
			}
			if (sb.length() > 0) {
				instance = sb.insert(0, name + ":{").append("}").toString();
			}
		}
		//
		return instance;
	}
}
/*
 **********************************************************************************
 * Copyright (C) 2017-2020 by KEYARK, INC. - All Rights Reserved.
 * All information contained herein is proprietary and confidential to KEYARK,
 * INC.
 * Any use, reproduction, or disclosure without the written
 * permission of KEYARK, INC. is prohibited.
 * Written by Andrey Belyaev <abelyaev@keyark.com>
 **********************************************************************************
 */