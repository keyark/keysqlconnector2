/*
 **********************************************************************************
 * Copyright (C) 2017-2020 by KEYARK, INC. - All Rights Reserved.
 * All information contained herein is proprietary and confidential to KEYARK, INC.
 * Any use, reproduction, or disclosure without the written
 * permission of KEYARK, INC. is prohibited.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
package com.keyark.keysqlconnector2.kobjects;

import java.util.Calendar;

/**
 * @author Konstantin Andreyev kandreyev@keyark.com
 *
 */
public class RandomDataProvider {
	private static final Double NULL_TO_NON_NULL_RATIO = 0.01;
	private static final Double DOUBLE_TO_INTEGER_RATIO = 0.1;
	private static final Integer INTEGER_MAX_VALUE = 10000;
	private static final Integer MAX_LINES_IN_STRING = 3;//5;
	private static final Integer MAX_WORD_LENGTH = 30;//45;
	private static final Integer MAX_WORDS_IN_LINE = 15;//20;
	private static final Double MULTILINE_TO_SINGLE_LINE_RATING = 0.1;

	/**
	 *
	 */
	public RandomDataProvider() {
	}

	public boolean isNull() {
		return Math.random() <= NULL_TO_NON_NULL_RATIO;
	}

	public boolean isDouble() {
		return Math.random() <= DOUBLE_TO_INTEGER_RATIO;
	}

	public boolean isMultiLine() {
		return Math.random() <= MULTILINE_TO_SINGLE_LINE_RATING;
	}

	/**
	 * @param parString
	 * @return
	 */
	public String randomizeString(String parString) {
		StringBuilder sb = new StringBuilder();
		if (parString != null) {
			String[] words = parString.split(" ");
			for (int i = 0; i < words.length; i++) {
				sb.append(randomString(words[i].length()));
				if (i < words.length - 1) {
					sb.append(" ");
				}
			}
		}
		//
		return parString == null ? null : sb.toString();
	}

	private int randomInt24() {
		return (int) Math.round(Math.random() * 16777215);
	}

	public long randomDate() {
		return Math.round(Math.random() * Calendar.getInstance().getTimeInMillis());
	}

	public int randomInt32() {
		return (int) Math.round(Math.random() * INTEGER_MAX_VALUE);
	}

	public long randomLong64() {
		return Math.round(Math.random() * Long.MAX_VALUE);
	}

	public double randomDouble() {
		return Math.random() * 10000.00;
	}

	//	public static String randomString(int lenght) {
	//		StringBuilder sb = new StringBuilder();
	//		while (sb.length() < lenght) {
	//			int ch = randomChar();
	//			if (Character.isAlphabetic(ch)) {
	//				sb.append(ch);
	//			}
	//		}
	//		return sb.toString();
	//	}
	//	public static String randomString(int lenght) {
	//		StringBuilder sb = new StringBuilder();
	//		while (sb.length() < lenght) {
	//			char ch = (char) Math.round(Math.random() * 127);
	//			if (Character.isLetterOrDigit(ch)) {
	//				sb.append(ch);
	//			}
	//		}
	//		//
	//		return sb.toString();
	//	}
	public static String randomString(int lenght) {
		int[] utfChars = new int[lenght];
		for (int i = 0; i < utfChars.length; i++) {
			utfChars[i] = randomUTF8AlphabeticCodepoint();
		}
		//
		return new String(utfChars, 0, utfChars.length);
	}

	//	public static String randomString(int lenght) {
	//		byte[] array = new byte[0xFFFF];
	//		new Random().nextBytes(array);
	//		String str = new String(array, Charset.forName("UTF-8"));
	//		StringBuilder sb = new StringBuilder();
	//		for (int i = 0; i < str.length(); i++) {
	//			if (Character.isLetterOrDigit(str.charAt(i))) {
	//				sb.append(str.charAt(i));
	//			}
	//			if (sb.length() >= lenght) {
	//				break;
	//			}
	//		}
	//		return new String(sb.toString().getBytes(Charset.forName("UTF-8")));
	//	}

	public static int randomUTF8AlphabeticCodepoint() {
		int cp = (int) Math.round(Math.random() * 0x1FFFFF);
		int num = (int) Math.round(Math.random() * 100);
		int upperLimit = 0x7F;
		if (num == 99) {
			upperLimit = 0xFFFF;
		} else if (num == 100) {
			upperLimit = 0x1FFFFF;
		}
		while (!Character.isAlphabetic(cp) || cp == 34 || Character.isWhitespace(cp)) {
			cp = (int) Math.round(Math.random() * upperLimit);
		}
		//
		return cp;
	}

	/**
	 * @return
	 */
	public String randomHexString24() {
		int minInt = 0x10000000;
		int random4byteInt = minInt + (int) Math.round(Math.random() * (Integer.MAX_VALUE - minInt));
		long minLong = 0x1000000000000000L;
		long random8bytelong = minLong + Math.round(Math.random() * (Long.MAX_VALUE - minLong));

		//
		return Long.toHexString(random8bytelong) + Integer.toHexString(random4byteInt);
	}

	/**
	 * @return
	 */
	public String randomYearString() {
		//
		return Long.toString(Math.round(Math.random() * 3000));
	}

	/**
	 * @return
	 */
	public String randomMultiLineString() {
		String randomString;
		if (isMultiLine()) {
			StringBuilder sb = new StringBuilder();
			int linesNum = (int) Math.round(Math.random() * MAX_LINES_IN_STRING);
			for (int i = 0; i < linesNum; i++) {
				if (sb.length() > 0) {
					sb.append("\\n");
				}
				sb.append(randomLine());
			}
			randomString = sb.toString();
		} else {
			randomString = randomString((int) Math.round(Math.random() * MAX_WORD_LENGTH));
		}
		//
		return randomString;
	}

	/**
	 * @return
	 */
	private String randomLine() {
		StringBuilder sb = new StringBuilder();
		int wordsNum = (int) Math.round(Math.random() * MAX_WORDS_IN_LINE);
		for (int i = 0; i < wordsNum; i++) {
			if (sb.length() > 0) {
				sb.append(" ");
			}
			sb.append(randomString((int) Math.round(Math.random() * MAX_WORD_LENGTH)));
		}
		//
		return sb.toString();
	}

}

/*
 **********************************************************************************
 * Copyright (C) 2017-2020 by KEYARK, INC. - All Rights Reserved.
 * All information contained herein is proprietary and confidential to KEYARK, INC.
 * Any use, reproduction, or disclosure without the written
 * permission of KEYARK, INC. is prohibited.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */