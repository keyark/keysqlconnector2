/*
 **********************************************************************************
 * Copyright (C) 2017-2020 by KEYARK, INC. - All Rights Reserved.
 * All information contained herein is proprietary and confidential to KEYARK, INC.
 * Any use, reproduction, or disclosure without the written
 * permission of KEYARK, INC. is prohibited.
 * Written by Andrey Belyaev <abelyaev@keyark.com>
 **********************************************************************************
*/
package com.keyark.keysqlconnector2.kobjects;

public enum ObjectType {
	Unknown, CharType, NumberType, DateType, Multiple, Composed
}
/*
 **********************************************************************************
 * Copyright (C) 2017-2020 by KEYARK, INC. - All Rights Reserved.
 * All information contained herein is proprietary and confidential to KEYARK,
 * INC.
 * Any use, reproduction, or disclosure without the written
 * permission of KEYARK, INC. is prohibited.
 * Written by Andrey Belyaev <abelyaev@keyark.com>
 **********************************************************************************
 */
