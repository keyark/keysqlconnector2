/*
 **********************************************************************************
 * Copyright (C) 2017-2020 by KEYARK, INC. - All Rights Reserved.
 * All information contained herein is proprietary and confidential to KEYARK, INC.
 * Any use, reproduction, or disclosure without the written
 * permission of KEYARK, INC. is prohibited.
 * Written by Andrey Belyaev <abelyaev@keyark.com>
 **********************************************************************************
*/
package com.keyark.keysqlconnector2.kobjects;

public enum PrimitiveObjectType {
	CharType, NumberType, DateType;

	static public String getType(PrimitiveObjectType type) {
		switch (type) {
		case CharType:
			return "CHAR";
		case NumberType:
			return "NUMBER";
		case DateType:
			return "DATE";
		}
		return "";
	}
}
/*
 **********************************************************************************
 * Copyright (C) 2017-2020 by KEYARK, INC. - All Rights Reserved.
 * All information contained herein is proprietary and confidential to KEYARK,
 * INC.
 * Any use, reproduction, or disclosure without the written
 * permission of KEYARK, INC. is prohibited.
 * Written by Andrey Belyaev <abelyaev@keyark.com>
 **********************************************************************************
 */
