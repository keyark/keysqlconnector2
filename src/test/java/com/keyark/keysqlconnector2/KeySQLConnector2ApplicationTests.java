/*
 **********************************************************************************
 * Copyright (C) 2017-present KEYARK, INC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Written by Konstantin Andreyev <kandreyev@keyark.com>
 **********************************************************************************
 */
package com.keyark.keysqlconnector2;

import com.keyark.UserIsNotAuthenticatedException;
import com.keyark.keysqlconnector2.kobjects.KObjectService;
import com.keyark.keysqlconnector2.kobjects.KeySQLObject;
import com.keyark.keysqlconnector2.kobjects.KeySqlRequestHelper;
import com.keyark.keysqlconnector2.kobjects.RandomDataProvider;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static org.junit.jupiter.api.Assertions.*;

public class KeySQLConnector2ApplicationTests implements TestConstants{
	private static final Logger logger = LoggerFactory.getLogger(KeySQLConnector2ApplicationTests.class);
	private static final int INSTANTS_NUM = 100000;
	//
	private static final int INSERT_STATEMENTS_SIZE_LIMIT = 100 * 1024;//100 kB - all insert statements will be executed at once until full size will breach this limit

	private static final String kObject = "MOVIE";

	private static final String XYZ_CONN_FILE = "xyz-conn.properties";
	private static final String LOCAL_CONN_FILE = "local-conn.properties";
	@Test
	public void pingPongTest() throws IOException, GeneralSecurityException {
		KSConnector connector = createConnector();
		assertTrue(connector.ping());
		connector.close();
	}
	@Test
	public void connectTest() throws IOException, GeneralSecurityException {
		String kusr = System.getProperty("kusr");
		String secret = System.getProperty("ksecret");
		KSConnector connector = createConnector();
		assertEquals(Constants.SERVER_REPLY_OK,connector.bindUser(kusr, secret));
		connector.close();
	}
	@Test
	public void reconnectTest() throws IOException, GeneralSecurityException {
		String kusr = System.getProperty("kusr");
		String secret = System.getProperty("ksecret");
		KSConnector connector = createConnector();
		assertEquals(Constants.SERVER_REPLY_OK,connector.bindUser(kusr, secret));
		assertEquals(Constants.SERVER_REPLY_OK,connector.reconnect(kusr));
		connector.close();
	}
	@Test
	public void changePwdTest() throws IOException, GeneralSecurityException {
		String kusr = System.getProperty("kusr");
		String secret = System.getProperty("ksecret");
		KSConnector connector = createConnector();
		assertEquals(Constants.SERVER_REPLY_OK,connector.bindUser(kusr, secret));
		assertEquals(Constants.SERVER_REPLY_OK,connector.changePassword(kusr, secret, secret));
		connector.close();
	}
	@Test
	public void logoutTest() throws IOException, GeneralSecurityException {
		final String kusr = System.getProperty("kusr");
		final String secret = System.getProperty("ksecret");
		final KSConnector connector = createConnector();
		assertThrows(UserIsNotAuthenticatedException.class,() -> { connector.logout(); });
		connector.close();
		final KSConnector connector2 = createConnector();
		assertEquals(Constants.SERVER_REPLY_OK,connector2.bindUser(kusr, secret));
		assertEquals(Constants.SERVER_REPLY_OK,connector2.logout());
		connector2.close();
	}
	@Test
	public void selectAbortionTest()
			throws IOException, GeneralSecurityException, ExecutionException, InterruptedException, TimeoutException {
		String kusr = System.getProperty("kusr");
		String secret = System.getProperty("ksecret");
		System.out.println(kusr + "/" + secret);
		//		Connector2 connector = new Connector2("72.52.84.130", 5569, true);
		KSConnector connector = createConnector();
		connector.bindUser(kusr, secret);
		if(!doesTestCatalogExist(connector)){
			ReplyMessage reply = connector.submit(CREATE_TEST_KEYSQL_CONNECTOR_SCRIPT, MessageFormat.keysql);
			if(!reply.isSuccess()){
				System.out.println(reply.getReply());
				fail("Failed to create test catalog");
			}
		};
		if(!doesTestStoreExist(connector)){
			ReplyMessage reply = connector.submit("CREATE STORE " + STORE + " FOR CATALOG " + CATALOG, MessageFormat.keysql);
			if(!reply.isSuccess()){
				System.out.println(reply.getReply());
				fail("Failed to create test store");
			}
		}
		fillTestStoreWithRandomData(connector);
		selectAbortionTest(500, connector);
		selectAbortionTest(1000, connector);
		selectAbortionTest(2000, connector);
		selectAbortionTest(3000, connector);
		//
		try{
			connector.submit("DROP STORE " + STORE, MessageFormat.keysql);
			connector.submit("DROP CATALOG " + CATALOG, MessageFormat.keysql);
		} catch (Throwable e) {
			String errorMsg = e.getClass().getSimpleName() + ": " + e.getMessage();
			logger.error(errorMsg);
		}
		//
		connector.close();
	}

	private void fillTestStoreWithRandomData(KSConnector connector)
			throws IOException, ExecutionException, InterruptedException, TimeoutException {
		RandomDataProvider randomProviderService = new RandomDataProvider();
		KObjectService kObjectService = new KObjectService(new KeySqlRequestHelper());
		//
		KeySQLObject keySQLObjectObject = kObjectService.getKeySQLObjFromCatalog(CATALOG, kObject, connector);
		int count = 0;
		int batchLength = 0;
		Collection<String> keySqlInstances = new ArrayList<String>();
		while (count < INSTANTS_NUM) {
			try {
				if (batchLength >= INSERT_STATEMENTS_SIZE_LIMIT) {
					kObjectService.executeInsertStatement(STORE, keySqlInstances, connector);
					keySqlInstances = new ArrayList<String>();
					batchLength = 0;
					logger.info("Inserted " + count + " instances out of " + INSTANTS_NUM);
				}
				String keySqlInstance = keySQLObjectObject.getKInstance(randomProviderService);
				if (keySqlInstance != null && !keySqlInstance.equals(keySQLObjectObject.getName() + ":NULL")) {
					keySqlInstances.add(keySqlInstance);
					batchLength += keySqlInstance.length();
					count++;
				}
				//} catch (InterruptedException ignore) {
			} catch (SocketException | SocketTimeoutException e) {
				throw e;
			} catch (IOException e) {
				String errorMsg = e.getClass().getSimpleName() + ": " + e.getMessage();
				logger.error(errorMsg);
				keySqlInstances = new ArrayList<String>();
				batchLength = 0;
			}
		} //while end
		if (keySqlInstances.size() > 0) {
			kObjectService.executeInsertStatement(STORE, keySqlInstances, connector);
			logger.info("Inserted " + count + " instances out of " + INSTANTS_NUM);
		}


	}

	private boolean doesTestStoreExist(KSConnector connector) throws IOException {
		ReplyMessage reply = connector.submit("SHOW STORE " + STORE, MessageFormat.json);
		//
		return reply.isSuccess();
	}

	private boolean doesTestCatalogExist(KSConnector connector) throws IOException {
	    ReplyMessage reply = connector.submit("SHOW CATALOG " + CATALOG, MessageFormat.json);
		//
		return reply.isSuccess();
    }

	/**
	 *  Abort select query
	 * @param abortDelay - delay in milliseconds before aborting the query
	 * @param connector - KSConnector instance
	 * @throws IOException - if an I/O error occurs
	 */
	@SuppressWarnings("resource")
	void selectAbortionTest(int abortDelay, KSConnector connector) throws IOException, InterruptedException {
		OutputStream os = new NullOutputStream();
		Thread closeOutputStreamThread = new Thread(new CloseOutputStream(os, abortDelay));
		closeOutputStreamThread.start();
		connector.submit("SELECT " + kObject + " FROM " + STORE, MessageFormat.json, os);
		ReplyMessage rmsg = connector.submit("SELECT COUNT(*) FROM " + STORE, MessageFormat.keysql);
		System.out.println(rmsg.getReply());
		//
		rmsg = connector.submit("SELECT * FROM " + STORE + " LIMIT 3", MessageFormat.keysql);
		System.out.println(rmsg.getReply());
		assertTrue(connector.ping());
		closeOutputStreamThread.join();
	}

	private static KSConnector createConnector() throws IOException, GeneralSecurityException {
		final String khost = System.getProperty("khost");
		System.out.println("khost: " + khost);
		final Properties connProps = new Properties();
		final String propsFilename = khost == null || !"localhost".equals(khost) ? XYZ_CONN_FILE : LOCAL_CONN_FILE;
		System.out.println("propsFilename: " + propsFilename);
		final URL resource = Thread.currentThread().getContextClassLoader().getResource(propsFilename);
		connProps.load(new FileInputStream(resource.getPath()));
		return new Connector2(connProps.getProperty("host")
				, Integer.valueOf(connProps.getProperty("port")), Boolean.valueOf(connProps.getProperty("tls")));
	}

	private static class CloseOutputStream implements Runnable {
		private OutputStream os;
		private final int sleepseconds;

		public CloseOutputStream(OutputStream os, int sleepseconds) {
			this.os = os;
			this.sleepseconds = sleepseconds;
		}

		@Override
		public void run() {
			//			System.out.println("CloseOutputStream started");
			try {
				Thread.sleep(sleepseconds);
				os.close();
				//				System.out.println("OutputStream has been closed.");
			} catch (@SuppressWarnings("unused") InterruptedException | IOException ignore) {
			}
		}

	}

	private static class NullOutputStream extends OutputStream {
		private boolean closed = false;

		@Override
		public void write(int b) throws IOException {
			if (closed) {
				throw new IOException("NullOutputStream has been closed");
			}
		}

		@Override
		public void close() throws IOException {
			closed = true;
		}
	}
}
