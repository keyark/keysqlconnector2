To access maven 'https://pkgs.dev.azure.com/keyark-shared/_packaging/keyark-shared/maven/v1'

1. add or edit the settings.xml file in ${user.home}/.m2

<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
                              https://maven.apache.org/xsd/settings-1.0.0.xsd">
  <servers>
    <server>
      <id>keyark-shared</id>
      <username>keyark-shared</username>
      <password>[PERSONAL_ACCESS_TOKEN]</password>
    </server>
  </servers>
</settings>

2. Request [PERSONAL_ACCESS_TOKEN] from kandreyev@keyark.com or from another Azure DevOps admin and paste it into the <password> tag
